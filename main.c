#include "utility.h"
#include <stdio.h>
#include <string.h>
const char *replaceString(const char *line, size_t len);

int main() {
  const char *test = "hei&der<borte>!";
  size_t size = strlen(test);
  const char *res = replaceString(test, size);
  printf("%s\n", res);
  free(res);
  return 0;
}
